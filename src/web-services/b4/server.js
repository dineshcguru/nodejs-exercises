const config = {
    bookdb:'http://localhost:5986/books/',
    b4db: 'http://localhost:5986/b4/'
};

const
    express = require('express'),
    app = express(),
    port = 3000;

app.listen(port, function() {
    console.log(`ready captain. Listening on port ${port}`);
});

//require('./lib/book-search.js')(config, app);
require('./lib/field-search.js')(config, app);
require('./lib/bundle.js')(config, app);