#! /usr/bin/env node
'use strict';
const
    express = require('express'),
    app = express(),
    port = 3000;

app.get('/api/:name', function(req, res) {
    res.status(200).json({"hello": req.params.name});
});
app.listen(port, function() {
    console.log(`ready captain. Listening on port ${port}`);
});