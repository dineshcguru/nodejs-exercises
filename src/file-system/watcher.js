const
    fs = require('fs');
    filename = process.argv[2];

if (!filename) {
    throw Error("A file to watch must be specified!");
}

if(fs.existsSync(filename)) {
    fs.watch(filename, function(){
        if(!fs.existsSync(filename)) {
            throw Error("File watched have been removed!");
        }
        console.log("File " + filename + "just changed!");
    })
} else {
    console.log("File " + filename + " does not exist");
}
console.log("Now watching " + filename + " for changes ...");