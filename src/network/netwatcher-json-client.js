"use strict";
const
    net = require('net'),
    ldj = require('./ldj.js'),
    netclient = net.connect({ port: 5432}),
    ldjclient = ldj.connect(netclient);

ldjclient.on('message', function(message) {
    console.log(message);
    if(message.type == 'watching') {
        console.log("Now watching: " + message.file);
    } else if (message.type == 'changed') {
        let date = new Date(message.timestamp);
        console.log("File '" + message.file + "' changed at " + date);
    } else {
        throw Error("Unrecognized message type: " + message.type);
    }
});