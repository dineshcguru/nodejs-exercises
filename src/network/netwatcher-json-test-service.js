"use strict";
const
    net = require('net'),
    server = net.createServer(function(connection) {
        //reporting
        console.log('Subscriber connected.');
        //send the first chunk immediately
        connection.write('{"type": "changed", "file": "targ');

        //after a second delay, send the other chunk
        let timer = setTimeout(function() {
            connection.write('et.txt", "timestamp" : 1578459290482}' + "\n");
            connection.end();
        }, 1000);

        //cleanup
        connection.on('end', function() {
            clearTimeout(timer);
            console.log('Subscriber disconnected.');
        });

    });


server.listen(5432, function() {
    console.log('Test Server Listening for subscribers...');
});